
package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

/**
 * @author camille_crocitorti
 *
 */
public class Shop {
	
	private Storage storage;
	private float cash;
	
	public Shop(Storage storage, float cash) {
		this.storage = storage;
		this.cash = cash;
		
	}
	
	
	public Item sell(String name) {
		if (this.storage.getItem(name).getName().equals("")) {
			System.out.println("erreur");
			return null;
			
		}
		
		// Comment remplacer le -1 par une variable. Exemple, 5 bananes sont vendues ?
		
		this.storage.getItem(name).setNbrElt((this.storage.getItem(name).getNbrElt())-1);
		this.cash += this.storage.getItem(name).getPrice();
		return this.storage.getItem(name);
	}
		
		
	public boolean ItemBuy (Item object) {
			float prix = object.getPrice();
			if (prix> this.cash) 
				return false;
			else {
				this.cash = this.cash - prix;
				return true;
		
		
	}

	}
}

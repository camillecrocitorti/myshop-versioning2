package com.ci.myShop.controller;

import java.util.HashMap;
import com.ci.myShop.model.Item;

public class Storage {
	private HashMap <String, Item > itemMap ;
	
	public Storage() {
		this.itemMap = new HashMap<String, Item>();
		
	}

	public void addItem(Item obj) {
		this.itemMap.put(obj.getName(), obj);
		
		
	}
	
	public Item getItem(String name) {
		if (this.itemMap.containsKey(name))
			return this.itemMap.get(name);
		
		else 
			return new Item(name, 0, 0, 0);
		

	}

}

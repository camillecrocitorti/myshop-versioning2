package com.ci.myShop.model;

/**
 * @author camille_crocitorti
 *
 */
public class Item {
	String name;
	int id;
	float price;
	int nbrElt;	


	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
		
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	


	public String display () {
		String item = "";
		item = String.format(this.getName() + " id : " + this.getId() + " price : " + this.getPrice() + " nbrElt : " + this.getNbrElt());
		return item;
		
		
	}
}
